FROM ubuntu
MAINTAINER Gadgetfactory

RUN apt-get update                                \
 && apt-get install --no-install-recommends --yes \
      x11-apps                                    \
      git                                         \
      wget                                        \
	  python-pip                                  \
	  libglib2.0-0                                \
#	  libsm6                                      \
#	  libxrender1                                 \
#	  libfreetype6                                \
#	  libfontconfig1                              \
 && pip install --upgrade pip                     \
 && pip install setuptools                        \
 && git clone https://github.com/GadgetFactory/fusesoc.git \
 && cd fusesoc                                    \
 && pip install -e .                              \ 
 && sh -c '/bin/echo -e "\n\n\n" | fusesoc init'  \
 && cd ..                                         \
# && pip install fusesoc                          \
 && apt-get clean autoclean                       \
 && apt-get autoremove --yes                      \
 && wget --no-check-certificate https://s3.amazonaws.com/gadgetfactory/xilinx/xil_compact.tgz \
 && tar -zxvf xil_compact.tgz --directory /       \
 && rm -rf xil_compact.tgz                        \                       
 && /bin/bash -c 'source /opt/Xilinx/14.7/ISE_DS/settings64.sh'

ENV PATH /opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64:$PATH
